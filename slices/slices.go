package slices

// Returns TRUE when the string 'needle' is in the given string slice 'slice'; Otherwise FALSE
func StringInSlice(needle string, slice []string) bool {
	for i := 0; i < len(slice); i++ {
		if slice[i] == needle {
			return true
		}
	}

	return false
}

// Returns TRUE when the string 'key' is in the given string slice 'set'; Otherwise FALSE
func StringKeyInMap(key string, set map[string]string) bool {
	for k, _ := range set {
		if k == key {
			return true
		}
	}

	return false
}

// Returns TRUE when the string 'value' is in the given string:string map 'set'; Otherwise FALSE
func StringValueInMap(value string, set map[string]string) bool {
	for _, v := range set {
		if v == value {
			return true
		}
	}

	return false
}

// Converts string slice 'slice' to an map string:string
// The Map will be fully filled with strings where a item in the slice represents the value and key in the map.
func StringSliceToMap(slice []string) map[string]string {
	set := make(map[string]string)

	for i := 0; i < len(slice); i++ {
		set[slice[i]] = slice[i]
	}

	return set
}
