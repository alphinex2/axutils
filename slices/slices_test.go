package slices

import (
	"gitlab.com/alphinex2/axtest/testgo"
	"testing"
)

func TestStringInSlice(t *testing.T) {
	var test []string = []string{
		"My",
		"Name",
		"Is",
		"Hello",
		"World",
	}

	testgo.IsBool(t, StringInSlice("Name", test), true)
	testgo.IsBool(t, StringInSlice("Matthias", test), false)
}

func TestStringKeyInMap(t *testing.T) {
	var test map[string]string = map[string]string{
		"Forename": "Brösel",
		"Title":    "The second",
		"Born":     "12.3.2008",
		"Company":  "FunFactory",
	}

	testgoIsBool(t, StringKeyInMap())
}
