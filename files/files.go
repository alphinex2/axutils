package files

import (
	"os"
)

// Returns TRUE when the file exists; Otherwise FALSE
func FileExists(path string) bool {
	_, err := os.Stat(path)

	if os.IsNotExist(err) {
		return false
	}

	return true
}
